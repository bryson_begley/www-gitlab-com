---
layout: handbook-page-toc
title: "Tweetdeck"
description: "Tweetdeck provides a platform to manage multiple Twitter accounts, schedule tweets, enable account sharing (via the Tweetdeck Teams feature), along with other advanced features."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### About Tweetdeck

Tweetdeck allows us to manage various GitLab twitter accounts, such as our [movingtogitlab Twitter account](https://twitter.com/movingtogitlab) and our [gitlabstatus Twitter account](https://twitter.com/GitLabStatus).

## How to Request

For Tweetdeck access, team members will need their own personal Twitter accounts. Those accounts will need to have 2FA authenticated. With manager approval, you can [submit an access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) to have your personal Twitter account added to Tweetdeck. 

Please include the following details:

- the link to your personal twitter profile
- a screenshot showing 2FA is activated (the screenshot should include a portion of your screen that shows your Twitter handle)
- the GitLab twitter handle you will help to manage
- justification on why you need access

## Resources

- [How to use Tweetdeck](https://help.twitter.com/en/using-twitter/how-to-use-tweetdeck)
- [About Tweetdeck Teams](https://help.twitter.com/en/using-twitter/tweetdeck-teams)
