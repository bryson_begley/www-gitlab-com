---
layout: handbook-page-toc
title: "Compensation and Leadership Development Committee"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Compensation and Leadership Development Committee Composition

- **Chairperson:** Sue Bostrom
- **Members:** Merline Saintil, Matthew Jacobson
- **Management DRI:** Chief People Officer

# Compensation and Leadership Development Committee Charter

THIS CHARTER WAS APPROVED BY THE BOARD ON 2021-03-18

1. **Purpose:** The purpose of the Compensation and Leadership Development Committee of the Board of Directors (the “**Board**”) of GitLab Inc. (the “**Company**”) is to assist the Board with compensation and other organization and people matters, including:
    - evaluating, recommending, approving and reviewing executive officer and director compensation arrangements, plans, policies and programs maintained by the Company; 
    - administering the Company’s cash- and equity-based compensation plans; and 
    - reviewing with management the Company’s organization and people activities. 

This charter (the “**Charter**”) sets forth the authority and responsibility of the Committee in fulfilling its purpose. 

1. **Structure and Membership:**
    - The Committee shall consist of two or more members of the Board, with the exact number determined by the Board. All members of the Committee will be appointed by the Board and will serve at the Board’s discretion. Members of the Committee may be replaced or removed by the Board at any time, with or without cause. Resignation or removal of a director from the Board, for whatever reason, will automatically constitute resignation or removal, as applicable, from the Committee. 
    - Members of the Committee must meet the criteria required by applicable law, the rules and regulations of the U.S. Securities and Exchange Commission (the “**Commission Rules**”) or such other qualifications as are established by the Board from time to time. 
    - Additionally, unless determined otherwise by the Board, each member of the Committee will be a “non-employee director” as defined in Rule 16b-3 promulgated under Section 16 of the Securities Exchange Act of 1934, as amended (the “**Exchange Act**”). 
    - The Board may appoint a member of the Committee to serve as the chairperson of the Committee (the “**Chair**”). If the Board does not appoint a Chair, the Committee members may designate a Chair by majority vote. The Chair will set the agenda for, preside over and conduct the proceedings of Committee meetings. 
1. **Duties and Responsibilities:** The principal duties and responsibilities of the Committee are set forth below. These responsibilities and duties are set forth as a guide, with the understanding that the Committee will carry them out in a manner that is appropriate given the Company’s needs and circumstances. The Committee may perform such other functions as are consistent with its purpose and applicable law, rules and regulations, as the Board may request or prescribe, or as the Committee deems necessary or appropriate consistent with its purpose.
    - *Compensation Matters:* 
        - i. Review the Company’s overall compensation strategy. 
        - ii. Review and approve the selection of the Company’s peer companies for compensation assessment purposes. 
        - iii. Review and approve the goals and objectives to be considered in determining the compensation of the Company’s Chief Executive Officer (the “**CEO**”) and all other executive officers of the Company who are members of the Company’s E-Group (collectively with the CEO, the “**Executive Officers**”)and evaluate their performance at least annually in light of these goals and objectives. 
        - iv. Review and approve all forms of compensation for the Executive Officers. The Committee may also make similar compensation related decisions with respect to other service providers of the Company if Board or Committee approval is required or desirable as determined by legal counsel. The CEO may not be present during voting or deliberations regarding the CEO’s compensation. 
        - v. Review, approve, amend or terminate any compensatory contracts or similar transactions or arrangements with the Company’s current or former Executive Officers. 
        - vi. Recommend to the Board, for determination by the Board, the form and amount of cash-based and equity-based compensation to be paid or awarded to the Company’s non-employee directors, including compensation for service on the Board or on committees of the Board. 
    - *Compensation Plans and Programs:* 
        - Review and approve or make recommendations to the Board with respect to adoption and approval of, or amendments to, the Company’s cash- and equity-based incentive compensation plans and arrangements and the cash amounts and aggregate numbers of shares reserved for issuance thereunder, respectively. 
        - Administer and interpret the Company’s cash- and equity-based compensation plans and agreements thereunder and in that capacity: 
            - i. approve all cash or equity awards thereunder (other than as delegated in the manners described in this Charter); 
            - 11. approve or recommend to the Board amendments to such plans (subject to stockholder approval when required) as may be necessary or appropriate to carry out the Company’s compensation strategy; 
            - iii. determine whether awards granted under the plans that have performance-related criteria have been earned; 
            - iv. correct any defect, supply any omission or reconcile any inconsistency in any cash- or equity-based compensation plan, award, exercise agreement or other arrangement; 
            - v. when appropriate, modify existing awards (with the consent of the grantees when necessary) and approve authorized exceptions to provisions of the plans; and 
            - vi. adopt any required or appropriate equity award timing policy. 
        - In addition to the authority to delegate to a subcommittee as set forth below, the Committee may delegate to the CEO (either alone or acting together with one or more officers of the Company), within the limits imposed by applicable law, the authority to make equity grants to service providers of the Company or of any subsidiary of the Company who are not the Executive Officers or directors of the Company, subject to guidelines or limits specified by the Committee; provided that, in the case of grants of stock options and stock appreciation rights, the price per share of any grant made pursuant to this delegated authority is no less than the fair market value of the Company’s common stock on the date of grant. 
        - Administer and, if deemed necessary, amend the Company’s 401(k) plan and any deferred compensation plans (collectively, the “Designated Plans”) and, if desired, delegate the routine administration of the Designated Plans to an administrative committee consisting of employees of the Company appointed by the Committee. 
        - Periodically review with the Chief Executive Officer and the Chairperson of the Board or lead independent director the succession plans for Executive Officers, reporting its findings and recommendations to the Board. If requested by the Board, the Committee will evaluate potential successors to executive officer positions and review strategies to accelerate the readiness of candidates identified for these positions, including an overall assessment of executive talent. The Committee also will review other leadership and management processes upon request of the Board. 
        - The Committee will review with management organization and people activities, which include, among other things, matters relating to the Company’s demographics; talent management and development; talent acquisition; employee engagement, retention and attrition; pay equity and diversity and inclusion. 
        - Oversee the Company’s submissions to stockholders on executive compensation matters, including advisory votes on executive compensation and the frequency of such votes, incentive and other executive compensation plans, and amendments to such plans. 
        - Review with management the Company’s major compensation-related risk exposures and the steps management has taken to monitor or mitigate such exposures. 
        - The Committee shall take such other action with respect to compensation matters as may be delegated from time to time by the Board. 
        - Discharge the responsibilities as set forth in such policies, codes and guidelines approved by the Board. 
1. **Meetings and Actions Without A Meeting:** 
    - The Committee shall meet in person or telephonically as often as it deems necessary in order to perform its responsibilities. A quorum of the Committee for the transaction of business will be a majority of its members. 
    - The Committee may also act by unanimous written consent in lieu of a meeting.
    - The Committee will maintain written minutes of its meetings and copies of its actions by written consent, and will file such minutes and copies of written consents with the minutes of the meetings of the Board. The Committee will regularly report to the Board on its activities. 
    - The Committee will regularly report to the Board on its activities. 
1. **Review of Committee Composition, Performance and Charter:** The Committee shall periodically review and reassess the adequacy of this Charter and recommend any proposed changes to the Board for approval. The Committee will evaluate on an annual basis the Committee’s composition and performance. 
1. **Compensation Consultants, Legal Counsel and Other Advisors:** The Committee may, in its sole discretion, retain, terminate or obtain the advice of compensation consultants, accountants, legal counsel, search firms, experts, or other advisors to assist the Committee in connection with its functions, including any studies or investigations. 
    - The Committee shall be directly responsible for the appointment, compensation and oversight of the work of any compensation consultant, legal counsel or other advisor retained by the Committee. 
    - In connection with the retention of such advisors (other than in-house legal counsel), the Committee will consider all factors related to the independence of such advisors, under the Commission Rules. Nothing herein requires that such advisors must be independent, only that the Committee consider the enumerated independence factors before selecting or otherwise receiving advice from such advisors. 
    - The Committee is empowered, without further action by the Board, to approve the fees and other retention terms of such advisors and may cause the Company to pay the compensation, as determined by the Committee, of any compensation consultant, legal counsel or other advisor retained by the Committee. 
1. **Delegation of Authority:** The Committee may from time to time, as it deems appropriate and to the extent permitted under applicable law, the Commission Rules and the Company’s Certificate of Incorporation and Bylaws, form and delegate, either exclusively or non-exclusively, authority to subcommittees. Subcommittees of the Committee will consist of one or more members of the Committee who will regularly report on their activities to the Committee. 
1. **Investigations:** The Committee shall have the authority to conduct or authorize investigations into any matters within the scope of its responsibilities as it shall deem appropriate, including the authority to request any officer, employee or advisor of the Company to meet with the Committee or any advisors engaged by the Committee.
