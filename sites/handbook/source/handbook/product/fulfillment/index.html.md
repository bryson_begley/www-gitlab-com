---
layout: markdown_page
title: How the Fulfillment section works
---

# On this page
{:.no_toc}

- TOC
{:toc}

# Overview

_coming soon_


# How the Fulfillment section works

_coming soon_

<!--
### Milestone planning process

_coming soon_

### How we prioritize

_coming soon_

### Peer issue review

_coming soon_
-->
# How Fulfillment collaborates with other groups

_coming soon_


# Customer subscription lifecycle

Customers interact with GitLab throughout the trial and purchasing journey in a variety of ways.  This section will capture that journey, which will evolve over time.  

## Current processes


### Trial

- [SaaS subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=138%3A97)
- [Self-managed subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=143%3A0)
- [Trial flow, documented by EntApps team](https://about.gitlab.com/handbook/business-technology/enterprise-applications/applications/#trial-web-direct-system-flow)


### New

- [SaaS subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=302%3A5577)
- [Self-managed subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=252%3A242)
- [New business flow, documented by EntApps team](https://about.gitlab.com/handbook/business-technology/enterprise-applications/applications/#new-business-system-flow)


### Additional seat purchase

- [SaaS subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=626%3A134)
- [Self-managed subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=626%3A222)


### Renewal

- [SaaS subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=96%3A59)
- [Self-managed subscription purchase, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=72%3A35)
- [Renewal Emails, documented by UX](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-gitlab-org-epics-3603?node-id=202%3A27)
- [Renewal notifications, documented by EntApps team](https://about.gitlab.com/handbook/business-technology/enterprise-applications/applications/#notifications)
- [Renewal system flow, documented by EntApps team](https://about.gitlab.com/handbook/business-technology/enterprise-applications/applications/#renewals-system-flow)


### Cancelation

- [UX flow documentation](https://www.figma.com/file/DCq7K8Srsv79tbH1yRkGbl/Document-user-flows-%5Bgitlab-org%2F-%2Fepics%2F3603%5D?node-id=28%3A28)


## Future iterations

We will be making updates to the current processes as the business needs evolve, and we learn from our customers' experiences. Here are some new projects that we are looking forward to:

1. [Updates to renewal customer messaging](https://gitlab.com/gitlab-org/gitlab/-/issues/322674)
1. Quarterly co-terms
1. Auto-renewals

